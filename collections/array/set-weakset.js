import chalk from 'chalk';

console.log('\n' + chalk.bgGreen.white.underline.bold('> SET:'));

console.log('Chars');
let chars = new Set(['🐷', '🐢', '🐷', '🐷']);
console.log(chars);

// Size
console.log('\n' + chalk.bgBlueBright.bold('Size'));
let size = chars.size;
console.log(size);

//Add
console.log('\n' + chalk.bgBlueBright.bold('Add'));

chars.add('char');
chars.add('🐼').add('🐦');

console.log(chars);

//Has
console.log('\n' + chalk.bgBlueBright.bold('Set has 🐦'));

let exist = chars.has('🐦');
console.log(exist);

//Delete
console.log('\n' + chalk.bgBlueBright.bold('Delete'));
chars.delete('🐦');
console.log(chars);


//Clear
console.log('\n' + chalk.bgBlueBright.bold('Clear'));

chars.clear();
console.log(chars);


// Looping
console.log('\n' + chalk.bgBlueBright.bold('Loop'));
let roles = new Set();
roles.add('admin')
  .add('editor')
  .add('subscriber');

for (let role of roles) {
  console.log(role);
}

console.log('\n' + chalk.bgBlueBright.bold('entries'));

console.log(roles.entries());
for (let [key, value] of roles.entries()) {
  console.log(key === value);
}

// WEAK SET - you cannot iterate elements of a WeakSet
console.log('\n' + chalk.bgGreen.white.underline.bold('> WEAK SET:'));
let computer = { type: 'laptop' };
let server = { type: 'server' };
let equipment = new WeakSet([computer, server]);

if (equipment.has(server)) {
  console.log('We have a server');
}