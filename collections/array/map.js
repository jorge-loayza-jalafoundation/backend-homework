import chalk from 'chalk';


// MAP = Map object holds key-value pairs where values of any type can be used as either keys or values.
console.log('\n' + chalk.bgGreen.white.underline.bold('> MAP:'));

let map1 = new Map([[1, 2], [2, 3], [4, 5]]);
console.log("Map1");
console.log(map1);

const map2 = new Map([["whole numbers", [1, 2, 3, 4]],
["Decimal numbers", [1.1, 1.2, 1.3, 1.4]],
["negative numbers", [-1, -2, -3, -4]]]);

console.log("Map2");
console.log(map2);

const map3 = new Map([[["first name", "last name"],
["sumit", "ghosh"]],
[["friend 1", "friend 2"],
["sourav", "gourav"]]]);

console.log("Map3");
console.log(map3);

// Size
console.log('\n' + chalk.bgBlueBright.bold('Size'));
console.log(`\nSize: ${map3.size}`);

// Add
console.log('\n' + chalk.bgBlueBright.bold('Add element'));

map1 = new Map();

map1.set("first name", "sumit");
map1.set("last name", "ghosh");
map1.set("website", "geeksforgeeks")
  .set("friend 1", "gourav")
  .set("friend 2", "sourav");

console.log(map1);

//Has key
console.log('\n' + chalk.bgBlueBright.bold('Has key'));
console.log("map1 has website ? " + map1.has("website"));

// Get value
console.log('\n' + chalk.bgBlueBright.bold('Get value'));

console.log("get value for key website " + map1.get("website"));

// Delete value
console.log('\n' + chalk.bgBlueBright.bold('Delete value'));
console.log("delete element with key website " + map1.delete("website"));
console.log(map1);

// Clear map
console.log('\n' + chalk.bgBlueBright.bold('Clear map'));
map1.clear();

console.log(map1);


// Values, keys entries
console.log('\n' + chalk.bgBlueBright.bold('Entries'));

let map = new Map();

map.set("first name", "sumit");
map.set("last name", "ghosh");
map.set("website", "geeksforgeeks")
  .set("friend 1", "gourav")
  .set("friend 2", "sourav");

const get_entries = map.entries();

console.log("----------entries---------");
for (var ele of get_entries)
  console.log(ele);

console.log('\n' + chalk.bgBlueBright.bold('Keys'));

const get_keys = map.keys();
console.log("--------keys----------");
console.log(get_keys);

console.log('\n' + chalk.bgBlueBright.bold('Values'));

const get_values = map.values();
console.log("----------values------------");
console.log(get_values);

for (var ele of get_values)
  console.log(ele);