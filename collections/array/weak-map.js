import chalk from 'chalk';


// WEAKMAP
/* 
A WeakMap is similar to a Map except the keys of a WeakMap must be objects
A WeakMap only has subset methods of a Map object:

    get(key)
    set(key, value)
    has(key)
    delete (key)

Here are the main difference between a Map and a WeekMap:

    Elements of a WeakMap cannot be iterated.
    Cannot clear all elements at once.
    Cannot check the size of a WeakMap.
*/
console.log('\n' + chalk.bgGreen.white.underline.bold('> WEAK MAP:'));

let weakMap = new WeakMap();

let obj = {};

// can't use a string as the key
// weakMap.set("test", "Whoops"); // Error, because "test" is not an object

weakMap.set(obj, "ok");

console.log(obj);

// Remove from memory
console.log('\n' + chalk.bgGreen.white.underline.bold('REMOVE FROM MEMORY:'));

let john = { name: "John" };

let weakMap2 = new WeakMap();

weakMap2.set(john, "...");
console.log(`WeakMap: `);
console.log(weakMap2);
// john = null;

console.log(`WeakMap: `);
console.log(weakMap2);


// Add
console.log('\n' + chalk.bgBlueBright.bold('Add element'));

let map1 = new WeakMap();
const obj1 = { name: "first name" };
const obj2 = { lastname: "lastname" };
map1.set(obj1, "sumit");
map1.set(obj2, "ghosh");

console.log(map1);

//Has key
console.log('\n' + chalk.bgBlueBright.bold('Has key'));
console.log("map1 has object ? " + map1.has(obj1));

// Get value
console.log('\n' + chalk.bgBlueBright.bold('Get value'));

console.log("get value for key website " + map1.get(obj2));

// Delete value
console.log('\n' + chalk.bgBlueBright.bold('Delete value'));
console.log("delete element with key website " + map1.delete(obj1));
console.log(map1);