import chalk from 'chalk';


// MULTIDIMENSIOINAL ARRAYS
console.log('\n' + chalk.bgGreen.white.underline.bold('> MULTIDIMENSIONAL:'));

let activities = [
  ['Work', 9],
  ['Eat', 1],
  ['Commute', 2],
  ['Play Game', 1],
  ['Sleep', 7]
];
console.log(activities);
console.table(activities);

//Get
console.log('\n' + chalk.bgBlueBright.bold('Get'));

console.log(activities[0][1]);

// Push
console.log('\n' + chalk.bgBlueBright.bold('Push'));

activities.push(['Study', 2]);

console.log(activities);

// Insert
console.log('\n' + chalk.bgBlueBright.bold('Insert'));

activities.splice(1, 0, ['Programming', 2]);

console.table(activities);

//Loop
console.log('\n' + chalk.bgBlueBright.bold('Loop'));

// loop the outer array
for (let i = 0; i < activities.length; i++) {
  // get the size of the inner array
  var innerArrayLength = activities[i].length;
  // loop the inner array
  for (let j = 0; j < innerArrayLength; j++) {
    console.log('[' + i + ',' + j + '] = ' + activities[i][j]);
  }
}

// activities.forEach((activity) => {
//   activity.forEach((data) => {
//     console.log(data);
//   });
// });