
import chalk from 'chalk';

// BASIC OPERATIONS
//   - Basic Array operations
//   - .find(), .map(), .filter(), etc., .flatMap(), .sort() others ?


const arr1 = ["Emil", "Tobias", "Linus"];
const arr2 = ["Cecilie", "Lone"];

// CONCAT
console.log('\n' + chalk.bgGreen.white.underline.bold('> CONCAT:'));
const arr3 = ["Robin"];
const children = arr1.concat(arr2, arr3)
console.log(children);


// FOR-OF, iterates over values that the `iterable object` defines to be iterated over.

console.log('\n' + chalk.bgGreen.white.underline.bold('> FOR-OF:'));

for (const element of arr1) {
  console.log(element);
}

const iterable = new Set([1, 1, 2, 2, 3, 3]);

for (const value of iterable) {
  console.log(value);
}

// FIND
console.log('\n' + chalk.bgGreen.white.underline.bold('> FIND:'));

const evergreens = [
  { name: "cedar", count: 2 },
  { name: "fir", count: 6 },
  { name: "pine", count: 3 }
];

console.log(evergreens);

const result = evergreens.find((tree, i) => {
  if (tree.count > 1 && i !== 0) return true;
});

console.log(result);

// MAP
// map((element) => { ... })
// map((element, index) => { ... })
// map((element, index, array) => { ... })

console.log('\n' + chalk.bgGreen.white.underline.bold('> MAP:'));

const myUsers = [
  { name: 'shark', likes: 'ocean' },
  { name: 'turtle', likes: 'pond' },
  { name: 'otter', likes: 'fish biscuits' }
]
console.log(myUsers);
const usersByLikes = myUsers.map(item => {
  const container = {};

  container[item.name] = item.likes;
  container.age = item.name.length * 10;

  return container;
})

console.log(usersByLikes);

// Filter
// filter((element) => { ... })
// filter((element, index) => { ... })
// filter((element, index, array) => { ... })

console.log('\n' + chalk.bgGreen.white.underline.bold('> FILTER:'));

const number = [112, 52, 0, -1, 944];
console.log(number);

const filtered = number.filter((num) => {
  return num > 0;
});

console.log(filtered);