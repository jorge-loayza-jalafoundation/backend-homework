import chalk from 'chalk';


// MULTIDIMENSIOINAL ARRAYS
console.log('\n' + chalk.bgGreen.white.underline.bold('> ADD - REMOVE:'));

let cats = ['Bob', 'Willy', 'Mini'];
// Pop
console.log('\n' + chalk.bgBlueBright.bold('Pop'));
console.log(cats);
cats.pop();
console.log(cats);

//Push
console.log('\n' + chalk.bgBlueBright.bold('Pop'));
console.log(cats);
cats.push('Puff', 'Shadow');
console.log(cats);

// Shift
console.log('\n' + chalk.bgBlueBright.bold('Shift'));
console.log(cats);
cats.shift();
console.log(cats);


// Unshift
console.log('\n' + chalk.bgBlueBright.bold('Unshift'));
console.log(cats);
cats.unshift('Spot', 'Cookie');
console.log(cats);

// Splice

console.log('\n' + chalk.bgBlueBright.bold('Splice'));
console.log(cats);
cats.splice(1,1);
console.log(cats);

// Filter
console.log('\n' + chalk.bgBlueBright.bold('Filter'));
console.log(cats);
const filteredArray = cats.filter((element) => {
  return element !== 'Puff';
});
console.log(filteredArray);
