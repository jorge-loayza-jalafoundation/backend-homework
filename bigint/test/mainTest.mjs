import { expect } from 'chai';
import { serialize, deserialize } from "../util/index.mjs";

const object = {
  number: 1234n,
  nestedObject: {
    number: 102938,
    bitint: 567890n
  },
  arr: [123n, 456n]
};

const resultJSON = '{"number":"1234n","nestedObject":{"number":102938,"bitint":"567890n"},"arr":["123n","456n"]}';

describe('Main test', function () {


  it('Should serialize object', function () {
    expect(serialize(object)).to.be.equal(resultJSON);
  })

  it('Should deserialize json', function () {
    expect(deserialize(resultJSON)).to.deep.equal(object);
  });

});