
function serialize(object) {
  const json = JSON.stringify(object, (key, value) =>
    typeof value === "bigint" ? value.toString() + "n" : value
  );
  return json;
}

export { serialize }