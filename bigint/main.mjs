
// Official method of Bitint serialization
// BigInt.prototype.toJSON = function () { return this.toString() + 'n' }

import { serialize, deserialize } from "./util/index.mjs";

const myObj = {
  testNumber: 123,
  testBigInt: 987n,
  nested: {
    myProp: 5n,
    myProp2: 10,
    myArray: [5n],
    myObject: {
      test: 5n
    }
  },
  myArray: [5, 50n]
}

console.log('Serialize object:');
console.log(serialize(myObj));
console.log(serialize({
  number: 1234n,
  nestedObject: {
    number: 102938,
    bitint: 567890n
  },
  arr: [123n, 456n]
}));

// Official method
// console.log(JSON.stringify(myObj));

const myJson = '{ "testNumber": 123, "testBigInt": "987n"}';

console.log('Deserialize JSON:');
console.log(deserialize(myJson));