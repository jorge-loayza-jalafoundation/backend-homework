import { inject, injectable } from 'inversify';
import { MessageBroker } from '../core/interfaceAdapters/messageBroker';
import amqp = require('amqplib');
import { Channel, Connection, ConsumeMessage, Options } from 'amqplib';
import config from '../config/config';
import { IDENTIFIERS } from '../identifiers';
import { IHttpClient, IHttpClientRequestParameters } from '../core/interfaceAdapters/httpClient';

@injectable()
export class RabbitMQService implements MessageBroker {
  conn: Connection;
  channel: Channel;
  q: string;
  constructor(
    @inject(IDENTIFIERS.HTTP_CLIENT)
    private httpClient: IHttpClient
  ) {}
  async setupConnection(): Promise<void> {
    this.conn = await amqp.connect({
      protocol: 'amqp',
      hostname: config.RABBITMQ_HOST,
      password: config.RABBITMQ_PASSWORD,
      username: config.RABBITMQ_USER,
      port: config.RABBITMQ_PORT,
    } as Options.Connect);
    console.log('⚡️[RabbitMQ]: Connected to instance.');

    this.channel = await this.conn.createChannel();
    for (const queue of config.QUEUES) {
      await this.channel.assertQueue(queue, { durable: false });
    }
  }

  async sendMessage(message: string, queueName: string): Promise<void> {
    if (this.channel.sendToQueue(queueName, Buffer.from(message))) {
      console.log('[RabbitMQ]: Send: %s', message);
    }
  }

  async consumeAttendanceQueue() {
    await this.channel.consume(config.QUEUE_ATTENDANCE, async (msg: ConsumeMessage) => {
      const result = msg.content.toString();
      const message = JSON.parse(result);
      console.log(`[RabbitMQ]: Received: ${result}`);
      const getParameters: IHttpClientRequestParameters<any> = {
        url: `${config.ATTENDANCE_SERVICE_HOST}:${config.ATTENDANCE_SERVICE_PORT}/attendance/${message.userId}`,
        requiresToken: false,
      };
      const requestResponse= await this.httpClient.get(getParameters);
      if (requestResponse.success) {
        const attendances = requestResponse.data as Array<any>;
        const totalAttendance = attendances.length;
        const messageToSend = JSON.stringify({userId: message.userId, totalAttendance})
        this.sendMessage(messageToSend, config.QUEUE_USER)
      } else {
        console.log(`[httpClient]: Error:`, requestResponse);
      }

      this.channel.ack(msg);
    });
  }
}