import { MessageBroker } from './core/interfaceAdapters/messageBroker';
import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import './ui/controllers/healthController';
import { IDENTIFIERS } from './identifiers';
import { AppContainer } from './container/appContainer';
import config from './config/config';

(async () => {
  const appContainer = new AppContainer();

  appContainer.init();

  await appContainer
    .get<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER)
    .setupConnection();
  
  await appContainer
    .get<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER)
    .consumeAttendanceQueue();

  appContainer
    .get<InversifyExpressServer>(IDENTIFIERS.INVERSIFY_APPLICATION)
    .build()
    .listen(config.API_PORT, () => {
      console.log(
        `⚡️[server]: Server is running at http://localhost:${config.API_PORT}`
      );
    });
})();
