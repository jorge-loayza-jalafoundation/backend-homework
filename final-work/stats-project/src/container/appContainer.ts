import express from 'express';
import { InversifyExpressServer } from 'inversify-express-utils';
import { IDENTIFIERS } from '../identifiers';
import { BaseContainer } from './baseContainer';
import { CommonModule } from './modules/commonModule';

export class AppContainer extends BaseContainer {
  constructor() {
    super({
      defaultScope: 'Singleton',
      skipBaseClassChecks: true,
    });
  }

  init(): void {
    this.provideCommonModule();
    this.provideInversifyExpressApplication();
  }


  private provideCommonModule(): void {
    this.load(new CommonModule());
  }

  private provideInversifyExpressApplication(): void {
    this.bind<InversifyExpressServer>(
      IDENTIFIERS.INVERSIFY_APPLICATION
    ).toConstantValue(
      new InversifyExpressServer(this).setConfig((app) => {
        app.use(
          express.urlencoded({
            extended: true,
          })
        );
        app.use(express.json());
      })
    );
  }
}
