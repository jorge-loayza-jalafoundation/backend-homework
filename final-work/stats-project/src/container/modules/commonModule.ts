import { RabbitMQService } from './../../infrastructure/RabbitMQService';
import { MessageBroker } from './../../core/interfaceAdapters/messageBroker';
import { interfaces } from 'inversify';
import { IDENTIFIERS } from '../../identifiers';
import { BaseModule } from './baseModule';
import { IHttpClient } from '../../core/interfaceAdapters/httpClient';
import { AxiosHttpClient } from '../../infrastructure/axiosHttpClient';

export class CommonModule extends BaseModule {
  constructor() {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideMessageBroker(bind);
    this.provideHttpClient(bind);
  }

  private provideMessageBroker(bind: interfaces.Bind) {
    bind<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER).to(RabbitMQService);
  }

  provideHttpClient(bind: interfaces.Bind) {
    bind<IHttpClient>(IDENTIFIERS.HTTP_CLIENT).to(AxiosHttpClient);
  }
}
