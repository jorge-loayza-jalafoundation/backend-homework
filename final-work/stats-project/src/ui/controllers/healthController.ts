import { IDENTIFIERS } from '../../identifiers';
import { MessageBroker } from './../../core/interfaceAdapters/messageBroker';
import * as express from 'express';
import { inject } from 'inversify';
import {
  interfaces,
  controller,
  httpGet,
  response,
} from 'inversify-express-utils';

@controller('/health')
export class HealthController implements interfaces.Controller {
  constructor(
    @inject(IDENTIFIERS.MESSAGE_BROKER)
    private messageBroker: MessageBroker
  ) {}
  @httpGet('/')
  private async health(@response() res: express.Response) {
    return res.status(200).json({ success: true });
  }
}
