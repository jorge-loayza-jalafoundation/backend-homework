export const IDENTIFIERS = {
  INVERSIFY_APPLICATION: Symbol('inversifyApplication'),
  HTTP_CLIENT: Symbol('httpClient'),
  MESSAGE_BROKER: Symbol('statsMessageBroker'),
};