export default {
  API_PORT: 3002,
  ATTENDANCE_SERVICE_HOST: 'http://localhost',
  ATTENDANCE_SERVICE_PORT: 3001,
  USER_SERVICE_HOST: 'http://localhost',
  USER_SERVICE_PORT: 3000,
  RABBITMQ_HOST: 'localhost',
  RABBITMQ_USER: 'user',
  RABBITMQ_PASSWORD: 'user',
  RABBITMQ_PORT: 5672,
  QUEUE_ATTENDANCE: 'attendance_updated',
  QUEUE_USER: 'update_user',
  QUEUES: ['attendance_updated', 'update_user'],
};
