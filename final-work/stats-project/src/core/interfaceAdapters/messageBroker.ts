export interface MessageBroker {
  setupConnection(): Promise<void>;
  sendMessage(message: string, queue: string): Promise<void>;
  consumeAttendanceQueue(): Promise<void>;
}