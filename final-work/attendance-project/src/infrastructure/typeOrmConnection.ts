import { injectable } from 'inversify';
import { ConnectionOptions, createConnection } from 'typeorm';
import config from '../config/config';
import { AttendanceEntity } from './dbEntities/attendance.entity';
import DbConnection from '../core/interfaceAdapters/db/dbConnection';

@injectable()
export class TypeOrmConnection implements DbConnection {
  async initialize(): Promise<void> {
    await createConnection({
      type: config.DB_TYPE,
      host: config.DB_HOST,
      port: config.DB_PORT,
      username: config.DB_USER,
      password: config.DB_PASSWORD,
      database: config.DB_NAME,
      entities: [AttendanceEntity],
      synchronize: config.DB_SYNCRO,
      logging: false,
      useUnifiedTopology: true,
      authSource: 'admin',
    } as ConnectionOptions)
      .then(async (connection) => {
        console.log(`⚡️[database]: Connected to db`);
      })
      .catch((error) => console.log('⚡️[database]: ', error));
  }
}
