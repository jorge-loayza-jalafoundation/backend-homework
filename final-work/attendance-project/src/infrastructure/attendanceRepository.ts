import { injectable } from 'inversify';
import { EntityRepository, getMongoRepository, getRepository, ObjectID } from 'typeorm';
import { AttendanceEntity } from './dbEntities/attendance.entity';
import { TypeOrmRepository } from './typeOrmRepository';

@injectable()
@EntityRepository(AttendanceEntity)
export class AttendanceTypeOrmRepository extends TypeOrmRepository<AttendanceEntity> {
  constructor() {
    super(getMongoRepository(AttendanceEntity));
  }

  async addAttendance(attendance: AttendanceEntity): Promise<AttendanceEntity> {
    const attendanceToSave = this.getRepository().create(attendance);
    const savedAttendance = await this.save(attendanceToSave);
    return savedAttendance;
  }

  async getAttendance(id: string): Promise<AttendanceEntity | undefined> {
    try {
      return await this.getRepository().findOne(id);
    } catch (error) {
      return undefined;
    }
  }

  async deleteAttendance(id: string): Promise<boolean> {
    const result = await this.getRepository().update(id, { isDeleted: true });
    return result.affected === 1 ? true : false;
  }
}
