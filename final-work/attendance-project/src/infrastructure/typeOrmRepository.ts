import { injectable, unmanaged } from 'inversify';
import { MongoRepository, Repository as OrmRepository } from 'typeorm';
import { Repository } from '../core/interfaceAdapters/repository';

@injectable()
export abstract class TypeOrmRepository<TEntity>
  implements Repository<TEntity>
{
  private readonly repository: MongoRepository<TEntity>;

  public constructor(@unmanaged() repository: MongoRepository<TEntity>) {
    this.repository = repository;
  }

  public async findAll(): Promise<TEntity[]> {
    return await this.repository.find();
  }

  public async findById(id: string): Promise<TEntity> {
    return await this.repository.findOne(id);
  }

  public async delete(id: string): Promise<boolean> {
    const result = await this.repository.delete(id);
    return !!result;
  }

  public async save(data: any) {
    const result = await this.repository.save(data);
    return result;
  }
  public getRepository(): MongoRepository<TEntity> {
    return this.repository;
  }
}
