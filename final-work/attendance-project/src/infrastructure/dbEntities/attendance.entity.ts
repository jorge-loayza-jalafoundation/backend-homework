import { Entity, Column, ObjectIdColumn } from 'typeorm';

@Entity('attendance')
export class AttendanceEntity {
  @ObjectIdColumn()
  id?: string;

  @Column()
  startTime: Date;

  @Column()
  endTime: Date;

  @Column()
  date: Date;

  @Column()
  notes: string;

  @Column()
  userId: string;

  @Column({ default: false })
  isDeleted: boolean;
}
