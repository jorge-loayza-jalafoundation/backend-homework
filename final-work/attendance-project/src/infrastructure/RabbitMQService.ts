import { inject, injectable } from 'inversify';
import { MessageBroker } from '../core/interfaceAdapters/messageBroker';
import amqp = require('amqplib');
import { Channel, Connection, ConsumeMessage, Options } from 'amqplib';
import config from '../config/config';
import { IDENTIFIERS } from '../identifiers';
import { AttendanceTypeOrmRepository } from './attendanceRepository';

@injectable()
export class RabbitMQService implements MessageBroker {
  conn: Connection;
  channel: Channel;
  q: string;
  constructor(
    @inject(IDENTIFIERS.ATTENDANCE_REPOSITORY)
    private attendanceRepository: AttendanceTypeOrmRepository
  ) {}
  async setupConnection(): Promise<void> {
    this.conn = await amqp.connect({
      protocol: 'amqp',
      hostname: config.RABBITMQ_HOST,
      password: config.RABBITMQ_PASSWORD,
      username: config.RABBITMQ_USER,
      port: config.RABBITMQ_PORT,
    } as Options.Connect);
    console.log('⚡️[RabbitMQ]: Connected to instance.');

    this.channel = await this.conn.createChannel();
    await this.channel.assertQueue(config.QUEUE_ATTENDANCE_CHANGED, {
      durable: false,
    });
  }

  async sendMessage(message: any): Promise<boolean> {
    if (
      this.channel.sendToQueue(
        config.QUEUE_ATTENDANCE_CHANGED,
        Buffer.from(message)
      )
    ) {
      console.log('[RabbitMQ]: Send: %s', message);
      return true;
    }
    return false;
  }

  async consumeUserDeleted() {
    await this.channel.consume(
      config.QUEUE_USER_DELETED,
      async (msg: ConsumeMessage) => {
        const result = msg.content.toString();
        console.log(`[RabbitMQ]: Received: ${result}`);
        const { userId } = JSON.parse(result);
        await this.attendanceRepository.getRepository().updateMany(
          { userId },
          { $set: { isDeleted: true } }
        );
        this.channel.ack(msg);
      }
    );
  }
}