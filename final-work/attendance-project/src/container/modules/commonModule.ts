import { RabbitMQService } from './../../infrastructure/RabbitMQService';
import { MessageBroker } from './../../core/interfaceAdapters/messageBroker';
import { interfaces } from 'inversify';
import { TypeOrmConnection } from '../../infrastructure/typeOrmConnection';
import DbConnection from '../../core/interfaceAdapters/db/dbConnection';
import { IDENTIFIERS } from '../../identifiers';
import { BaseModule } from './baseModule';

export class CommonModule extends BaseModule {
  constructor() {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideOrm(bind);
    this.provideMessageBroker(bind);
  }

  private provideOrm(bind: interfaces.Bind): void {
    bind<DbConnection>(IDENTIFIERS.DATABASE_CONNECTION).to(TypeOrmConnection);
  }

  private provideMessageBroker(bind: interfaces.Bind) {
    bind<MessageBroker>(IDENTIFIERS.ATTENDANCE_MESSAGE_BROKER).to(RabbitMQService);
  }
}
