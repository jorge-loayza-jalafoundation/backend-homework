import { interfaces } from 'inversify';
import { IDENTIFIERS } from '../../identifiers';
import { BaseModule } from './baseModule';
import { AttendanceTypeOrmRepository } from '../../infrastructure/attendanceRepository';
import { TypeOrmRepository } from '../../infrastructure/typeOrmRepository';
import { AttendanceEntity } from '../../infrastructure/dbEntities/attendance.entity';
import { AttendanceService } from '../../core/application/attendanceService';

export class AttendanceModule extends BaseModule {
  constructor() {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideAttendanceRepository(bind);
    this.provideAttendanceService(bind);
  }

  provideAttendanceService(bind: interfaces.Bind) {
    bind<AttendanceService>(IDENTIFIERS.ATTENDANCE_SERVICE).to(AttendanceService);
  }

  private provideAttendanceRepository(bind: interfaces.Bind): void {
    bind<TypeOrmRepository<AttendanceEntity>>(IDENTIFIERS.ATTENDANCE_REPOSITORY).to(
      AttendanceTypeOrmRepository
    );
  }
}
