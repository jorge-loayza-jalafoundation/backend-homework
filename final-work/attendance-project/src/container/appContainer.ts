import express from 'express';
import { InversifyExpressServer } from 'inversify-express-utils';
import { IDENTIFIERS } from '../identifiers';
import { BaseContainer } from './baseContainer';
import { CommonModule } from './modules/commonModule';
import { AttendanceModule } from './modules/attendanceModule';

export class AppContainer extends BaseContainer {
  constructor() {
    super({
      defaultScope: 'Singleton',
      skipBaseClassChecks: true,
    });
  }

  init(): void {
    this.provideCommonModule();
    this.provideInversifyExpressApplication();
    this.provideAttendanceModule();
  }


  private provideCommonModule(): void {
    this.load(new CommonModule());
  }

  private provideAttendanceModule(): void {
    this.load(new AttendanceModule());
  }

  private provideInversifyExpressApplication(): void {
    this.bind<InversifyExpressServer>(
      IDENTIFIERS.INVERSIFY_APPLICATION
    ).toConstantValue(
      new InversifyExpressServer(this).setConfig((app) => {
        app.use(
          express.urlencoded({
            extended: true,
          })
        );
        app.use(express.json());
      })
    );
  }
}
