import { Response } from './../interfaceAdapters/response';
import { inject, injectable } from 'inversify';
import { Brackets, FindConditions, FindManyOptions, Like } from 'typeorm';
import { AttendanceEntity } from '../../infrastructure/dbEntities/attendance.entity';
import { AttendanceTypeOrmRepository } from '../../infrastructure/attendanceRepository';
import { IDENTIFIERS } from '../../identifiers';
import { CreateAttendanceDto } from '../interfaceAdapters/createAttendanceDto';
import { MessageBroker } from '../interfaceAdapters/messageBroker';

@injectable()
export class AttendanceService {
  constructor(
    @inject(IDENTIFIERS.ATTENDANCE_REPOSITORY)
    private attendanceRepository: AttendanceTypeOrmRepository,
    @inject(IDENTIFIERS.ATTENDANCE_MESSAGE_BROKER)
    private messageBroker: MessageBroker
  ) {}

  async createAttendance(attendance: CreateAttendanceDto, userId: string) {
    try {
      this.messageBroker.sendMessage(JSON.stringify({ userId }));
      const attendanceSaved = await this.attendanceRepository.addAttendance({
        startTime: attendance.startTime,
        endTime: attendance.endTime,
        date: attendance.date,
        notes: attendance.notes,
        userId,
        isDeleted: false,
      });
      return {
        success: true,
        statusCode: 201,
        message: 'Attendance created.',
        data: {
          id: attendanceSaved.id,
          startTime: attendanceSaved.startTime,
          endTime: attendanceSaved.endTime,
          date: attendanceSaved.date,
          notes: attendanceSaved.notes,
          userId: attendanceSaved.userId,
        },
      } as Response<AttendanceEntity>;
    } catch (error) {
      console.log(`[server]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }
  async getAttendances(userId: string) {
    try {
      const attendances = await this.attendanceRepository.getRepository().find({
        where: {
          userId: { $eq: userId },
          isDeleted: { $eq: false },
        },
      });
      return {
        success: true,
        statusCode: 200,
        data: attendances,
      };
    } catch (error) {
      console.log(`[database]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }
  async deleteAttendance(id: string, userId: string) {
    if (!userId || typeof userId !== 'string') {
      return {
        success: false,
        statusCode: 400,
        message: 'Bad request.',
      };
    }
    try {
      const attendance = await this.attendanceRepository.getAttendance(id);
      if (!attendance) {
        return {
          success: false,
          statusCode: 404,
          message: 'Attendance not found.',
        };
      }
      if (attendance.userId !== userId) {
        return {
          success: false,
          statusCode: 400,
          message:
            'Only the user who created the attendance can delete the attendance.',
        };
      }
      this.messageBroker.sendMessage(JSON.stringify({ userId }));
      const attendanceUpdated =
        await this.attendanceRepository.deleteAttendance(id);
      if (attendanceUpdated) {
        return {
          success: true,
          statusCode: 200,
          message: 'Attendance deleted.',
        };
      } else {
        return {
          success: false,
          statusCode: 404,
          message: 'Attendance not found.',
        };
      }
    } catch (error) {
      console.log(`[database]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }

  async deleteUserAttendances(id: string): Promise<Response<any>> {
    const result = await this.attendanceRepository
      .getRepository()
      .updateMany({ userId: id }, { $set: { isDeleted: true } });
    return {
      success: true,
      message: `User's attendances deleted`,
      statusCode: 200
    }
  }
}
