import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsNotEmpty, IsString } from 'class-validator';

export class CreateAttendanceDto {
  @IsDefined()
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  startTime: Date;

  @IsDefined()
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  endTime: Date;

  @IsDefined()
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  date: Date;

  @IsDefined()
  @IsNotEmpty()
  @IsString()
  notes: string;
}