export default interface DbConnection {
  initialize(): Promise<void>;
}