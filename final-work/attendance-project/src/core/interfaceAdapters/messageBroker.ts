export interface MessageBroker {
  setupConnection(): Promise<void>;
  sendMessage(message: any): Promise<boolean>;
  consumeUserDeleted(): Promise<void>;
}