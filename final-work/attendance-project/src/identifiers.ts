export const IDENTIFIERS = {
  DATABASE_CONNECTION: Symbol('databaseConnection'),
  INVERSIFY_APPLICATION: Symbol('inversifyApplication'),
  ATTENDANCE_REPOSITORY: Symbol('attendanceRepository'),
  ATTENDANCE_MESSAGE_BROKER: Symbol('attendanceMessageBroker'),
  ATTENDANCE_SERVICE: Symbol('attendanceService')
}