import { AttendanceService } from './../../core/application/attendanceService';
import { Response } from './../../core/interfaceAdapters/response';
import * as express from 'express';
import {
  interfaces,
  controller,
  httpGet,
  response,
  queryParam,
  requestParam,
  httpPost,
  request,
  httpDelete,
} from 'inversify-express-utils';
import { plainToClass, plainToInstance } from 'class-transformer';
import { CreateAttendanceDto } from '../../core/interfaceAdapters/createAttendanceDto';
import { validate } from 'class-validator';
import { IDENTIFIERS } from '../../identifiers';
import { inject } from 'inversify';

export interface Attendance {
  id: string;
  startTime: Date;
  endDate: Date;
  date: Date;
  notes: string;
}

@controller('/attendance')
export class AttendanceController implements interfaces.Controller {
  constructor(
    @inject(IDENTIFIERS.ATTENDANCE_SERVICE)
    private attendanceService: AttendanceService
  ) {}

  @httpPost('/:id')
  private async createAttendance(
    @requestParam('id') id: string,
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    const attendance = plainToInstance(CreateAttendanceDto, req.body);
    const validationErrors = await validate(attendance, {
      forbidUnknownValues: true,
      validationError: { target: false },
    });

    if (validationErrors.length > 0) {
      return res.status(400).json({
        success: false,
        errors: validationErrors,
      });
    }

    const result = await this.attendanceService.createAttendance(
      attendance,
      id
    );

    return res.status(result.statusCode).json(result);
  }

  @httpGet('/:id')
  private async getUserAttendances(
    @requestParam('id') id: string,
    @response() res: express.Response
  ) {
    const result = await this.attendanceService.getAttendances(id);
    return res.status(200).json({
      success: true,
      statusCode: 200,
      message: 'OK',
      data: result.data,
    } as Response<[]>);
  }

  @httpDelete('/:id')
  private async deleteAttendance(
    @requestParam('id') id: string,
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    const { userId } = req.body;
    const result = await this.attendanceService.deleteAttendance(id, userId);
    return res.status(result.statusCode).json(result);
  }

  @httpDelete('/user/:id')
  private async deleteuserAttendances(
    @requestParam('id') id: string,
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    const result = await this.attendanceService.deleteUserAttendances(id);
    return res.status(result.statusCode).json(result);
  }
}
