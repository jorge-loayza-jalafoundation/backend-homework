import { MessageBroker } from './core/interfaceAdapters/messageBroker';
import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import './ui/controllers/attendanceController';
import './ui/controllers/healthController';
import DbConnection from './core/interfaceAdapters/db/dbConnection';
import { IDENTIFIERS } from './identifiers';
import { AppContainer } from './container/appContainer';
import config from './config/config';

(async () => {
  const appContainer = new AppContainer();

  appContainer.init();

  await appContainer
    .get<DbConnection>(IDENTIFIERS.DATABASE_CONNECTION)
    .initialize();

  await appContainer
    .get<MessageBroker>(IDENTIFIERS.ATTENDANCE_MESSAGE_BROKER)
    .setupConnection();
  
  // await appContainer
  //   .get<MessageBroker>(IDENTIFIERS.ATTENDANCE_MESSAGE_BROKER)
  //   .consumeUserDeleted();
  
  await appContainer.get<MessageBroker>(IDENTIFIERS.ATTENDANCE_MESSAGE_BROKER);
  appContainer
    .get<InversifyExpressServer>(IDENTIFIERS.INVERSIFY_APPLICATION)
    .build()
    .listen(config.API_PORT, () => {
      console.log(
        `⚡️[server]: Server is running at http://localhost:${config.API_PORT}`
      );
    });
})();
