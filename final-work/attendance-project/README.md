# Attendance - Attendance service

# Run de app in development mode

## Local Database with Docker

One way to run the local database is with Docker and the following tools are required:
* Docker
* Docker compose

To run the MySQL container just change the environment variables `MYSQL_ROOT_PASSWORD` and `MYSQL_DATABASE` of the file `local_db.yaml`, also you can configure the local port and container port in the section `ports`
on the same file.


After set up the variable just run the following command in the `local_db.yaml` file location:

```bash
$ docker-compose -f local_db.yaml up -d
```

## Run the project in dev mode

Before to tun the project be sure to set up the variables `DB_NAME`, `DB_USER`, and `DB_PASSWORD` according 
your database instance in the file `config/index.ts`.

To run the project in development mode just enter the following command:

```bash
$ npm run start:dev
```