export const IDENTIFIERS = {
  DATABASE_CONNECTION: Symbol('databaseConnection'),
  HTTP_CLIENT: Symbol('httpClient'),
  INVERSIFY_APPLICATION: Symbol('inversifyApplication'),
  USER_REPOSITORY: Symbol('userRepository'),
  USER_SERVICE: Symbol('userService'),
  MESSAGE_BROKER: Symbol('userMessageBroker'),
};