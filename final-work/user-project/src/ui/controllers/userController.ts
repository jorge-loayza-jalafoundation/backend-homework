import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import * as express from 'express';
import { inject } from 'inversify';
import {
  interfaces,
  controller,
  httpGet,
  request,
  response,
  next,
  httpPost,
  httpDelete,
  requestParam,
  queryParam,
  results,
} from 'inversify-express-utils';
import { UserService } from '../../core/application/userService';
import { CreateUserDto } from '../../core/interfaceAdapters/createUserDto';
import { IDENTIFIERS } from '../../identifiers';

@controller('/users')
export class UserController implements interfaces.Controller {
  constructor(
    @inject(IDENTIFIERS.USER_SERVICE) private userService: UserService
  ) {}

  @httpPost('/')
  private async createUser(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    const user = plainToClass(CreateUserDto, req.body);
    const validationErrors = await validate(user, {
      forbidUnknownValues: true,
      validationError: { target: false },
    });

    if (validationErrors.length > 0) {
      return res.status(400).json({
        success: false,
        errors: validationErrors,
      });
    }

    const result = await this.userService.createUser(user);

    return res.status(result.statusCode).json(result);
  }

  @httpDelete('/:id')
  private async deleteUser(
    @requestParam('id') id: string,
    @response() res: express.Response
  ) {
    const result = await this.userService.deleteUser(id);
    return res.status(result.statusCode).json(result);
  }

  @httpGet('/')
  private async getUsers(@response() res: express.Response, @queryParam("name") name: string, @queryParam("nickName") nickName: string) {
    const result = await this.userService.getUsers(name, nickName);
    return res.status(result.statusCode).json(result);
  }
}
