import * as express from 'express';
import { inject } from 'inversify';
import {
  interfaces,
  controller,
  httpGet,
  response,
  requestParam,
} from 'inversify-express-utils';
import { UserService } from '../../core/application/userService';
import { IDENTIFIERS } from '../../identifiers';

@controller('/report')
export class UserReportController implements interfaces.Controller {
  constructor(
    @inject(IDENTIFIERS.USER_SERVICE) private userService: UserService
  ) {}

  @httpGet('/:id')
  private async getUsers(
    @requestParam('id') id: string,
    @response() res: express.Response,
  ) {
    const result = await this.userService.getUserReport(id);
    return res.status((await result).statusCode).json(result);
  }
}
