import { interfaces } from 'inversify';
import { TypeOrmConnection } from '../../infrastructure/typeOrmConnection';
import DbConnection from '../../core/interfaceAdapters/db/dbConnection';
import { IDENTIFIERS } from '../../identifiers';
import { BaseModule } from './baseModule';
import { UserTypeOrmRepository } from '../../infrastructure/userRepository';
import { TypeOrmRepository } from '../../infrastructure/typeOrmRepository';
import { UserEntity } from '../../infrastructure/dbEntities/user.entity';
import { UserService } from '../../core/application/userService';

export class UserModule extends BaseModule {
  constructor() {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideUserRepository(bind);
    this.provideUserService(bind);
  }

  provideUserService(bind: interfaces.Bind) {
    bind<UserService>(IDENTIFIERS.USER_SERVICE).to(UserService);
  }

  private provideUserRepository(bind: interfaces.Bind): void {
    bind<TypeOrmRepository<UserEntity>>(IDENTIFIERS.USER_REPOSITORY).to(
      UserTypeOrmRepository
    );
  }
}
