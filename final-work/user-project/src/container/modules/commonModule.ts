import { AxiosHttpClient } from './../../infrastructure/axiosHttpClient';
import { IHttpClient } from './../../core/interfaceAdapters/httpClient';
import { interfaces } from 'inversify';
import { TypeOrmConnection } from '../../infrastructure/typeOrmConnection';
import DbConnection from '../../core/interfaceAdapters/db/dbConnection';
import { IDENTIFIERS } from '../../identifiers';
import { BaseModule } from './baseModule';
import { MessageBroker } from '../../core/interfaceAdapters/messageBroker';
import { RabbitMQService } from '../../infrastructure/RabbitMQService';

export class CommonModule extends BaseModule {
  constructor() {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideOrm(bind);
    this.provideHttpClient(bind);
    this.provideMessageBroker(bind);
  }

  private provideOrm(bind: interfaces.Bind): void {
    bind<DbConnection>(IDENTIFIERS.DATABASE_CONNECTION).to(TypeOrmConnection);
  }

  provideHttpClient(bind: interfaces.Bind) {
    bind<IHttpClient>(IDENTIFIERS.HTTP_CLIENT).to(AxiosHttpClient);
  }

  private provideMessageBroker(bind: interfaces.Bind) {
    bind<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER).to(RabbitMQService);
  }
}
