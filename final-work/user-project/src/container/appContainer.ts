import express from 'express';
import { InversifyExpressServer } from 'inversify-express-utils';
import { IDENTIFIERS } from '../identifiers';
import { BaseContainer } from './baseContainer';
import { CommonModule } from './modules/commonModule';
import { UserModule } from './modules/userModule';

export class AppContainer extends BaseContainer {
  constructor() {
    super({
      defaultScope: 'Singleton',
      skipBaseClassChecks: true,
    });
  }

  init(): void {
    this.provideCommonModule();
    this.provideInversifyExpressApplication();
    this.provideUserModule();
  }


  private provideCommonModule(): void {
    this.load(new CommonModule());
  }

  private provideUserModule(): void {
    this.load(new UserModule());
  }

  private provideInversifyExpressApplication(): void {
    this.bind<InversifyExpressServer>(
      IDENTIFIERS.INVERSIFY_APPLICATION
    ).toConstantValue(
      new InversifyExpressServer(this).setConfig((app) => {
        app.use(
          express.urlencoded({
            extended: true,
          })
        );
        app.use(express.json());
      })
    );
  }
}
