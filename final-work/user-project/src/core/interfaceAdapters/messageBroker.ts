export interface MessageBroker {
  setupConnection(): Promise<void>;
  sendMessage(message: string, queue: string): Promise<boolean>;
  consumeUpdateUser(): Promise<void>;
}