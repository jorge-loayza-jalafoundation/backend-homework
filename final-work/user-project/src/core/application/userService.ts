import { Response } from './../interfaceAdapters/response';
import { IHttpClientRequestParameters } from './../interfaceAdapters/httpClient';
import { inject, injectable } from 'inversify';
import { UserTypeOrmRepository } from '../../infrastructure/userRepository';
import { IDENTIFIERS } from '../../identifiers';
import { CreateUserDto } from '../interfaceAdapters/createUserDto';
import { IHttpClient } from '../interfaceAdapters/httpClient';
import config from '../../config/config';
import { MessageBroker } from '../interfaceAdapters/messageBroker';

@injectable()
export class UserService {
  constructor(
    @inject(IDENTIFIERS.USER_REPOSITORY)
    private userRepository: UserTypeOrmRepository,
    @inject(IDENTIFIERS.HTTP_CLIENT)
    private httpClient: IHttpClient,
    @inject(IDENTIFIERS.MESSAGE_BROKER)
    private messageBroker: MessageBroker
  ) {}

  async createUser(user: CreateUserDto) {
    try {
      const userSaved = await this.userRepository.addUser({
        firstName: user.firstName,
        lastName: user.lastName,
        nickName: user.nickName,
      });
      return {
        success: true,
        statusCode: 201,
        message: 'User created.',
        data: {
          id: userSaved.id,
          firstName: userSaved.firstName,
          lastName: userSaved.lastName,
          nickName: userSaved.nickName,
        },
      };
    } catch (error) {
      console.log(`[database]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }

  async deleteUser(id: string) {
    try {
      // const messageSend = this.messageBroker.sendMessage(
      //   JSON.stringify({ userId: id }),
      //   config.QUEUE_USER_DELETED
      //   );

      // if (!messageSend) {
      //   return {
      //     success: true,
      //     statusCode: 500,
      //     message: 'Internal server error.',
      //   };
      // }
      const getParameters: IHttpClientRequestParameters<any> = {
        url: `${config.ATTENDANCE_HOST}:${config.ATTENDANCE_PORT}/attendance/user/${id}`,
        requiresToken: false,
      };
      const requestResponse: Response<[]> = await this.httpClient.delete<
        Response<[]>
        >(getParameters);
      
      if (requestResponse.success) {
        console.log('deleted attendances');
        
        const userIsDeleted = await this.userRepository.deleteUser(id);
        if (userIsDeleted) {
          return {
            success: true,
            statusCode: 200,
            message: 'User deleted.',
          };
        } else {
          return {
            success: false,
            statusCode: 404,
            message: 'User not found.',
          };
        }  
      }
    } catch (error) {
      console.log(`[database]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }

  async getUsers(name: string, nickName: string) {
    try {
      const users = await this.userRepository.getUsers(name, nickName);
      return {
        success: true,
        statusCode: 200,
        data: {
          users,
        },
      };
    } catch (error) {
      console.log(`[database]: `, error);
      return {
        success: false,
        statusCode: 500,
      };
    }
  }

  async getUserReport(id: string): Promise<Response<any>> {
    if (!id) {
      return {
        success: false,
        statusCode: 400,
        message: 'Bad Request.',
      } as Response<{}>;
    }

    try {
      const userExist = await this.userRepository.userExist(id);
      if (!userExist) {
        return {
          success: false,
          statusCode: 404,
          message: 'User Not Found.',
        } as Response<{}>;
      }

      const getParameters: IHttpClientRequestParameters<any> = {
        url: `${config.ATTENDANCE_HOST}:${config.ATTENDANCE_PORT}/attendance/${id}`,
        requiresToken: false,
      };
      const requestResponse: Response<[]> = await this.httpClient.get<
        Response<[]>
      >(getParameters);
      if (requestResponse.success) {
        const user = await this.userRepository.getUser(id);
        return {
          success: true,
          statusCode: 200,
          message: 'OK',
          data: {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            nickName: user.nickName,
            totalAttendance: user.totalAttendance,
            attendanceList: requestResponse.data,
          },
        } as Response<{}>;
        console.log('Result: ', requestResponse);
      } else {
        console.log(`[httpClient]: Error:`, requestResponse);
        return {
          success: false,
          statusCode: 500,
          message: 'Internal server error.',
        } as Response<{}>;
      }
    } catch (error) {
      console.log(`[httpClient]: Error`, error);
      return {
        success: false,
        statusCode: 500,
        message: 'Internal server error.',
      } as Response<{}>;
    }
  }
}
