import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import './ui/controllers/healthController';
import './ui/controllers/userController';
import './ui/controllers/userReportController';
import DbConnection from './core/interfaceAdapters/db/dbConnection';
import { IDENTIFIERS } from './identifiers';
import { AppContainer } from './container/appContainer';
import config from './config/config';
import { MessageBroker } from './core/interfaceAdapters/messageBroker';

(async () => {
  const appContainer = new AppContainer();
  appContainer.init();

  await appContainer
    .get<DbConnection>(IDENTIFIERS.DATABASE_CONNECTION)
    .initialize();
    
  await appContainer
    .get<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER)
    .setupConnection();

  await appContainer.get<MessageBroker>(IDENTIFIERS.MESSAGE_BROKER).consumeUpdateUser();
  
  appContainer
    .get<InversifyExpressServer>(IDENTIFIERS.INVERSIFY_APPLICATION)
    .build()
    .listen(config.API_PORT, () => {
      console.log(
        `⚡️[server]: Server is running at http://localhost:${config.API_PORT}`
      );
    });
})();
