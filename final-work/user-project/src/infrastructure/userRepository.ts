import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { UserEntity } from './dbEntities/user.entity';
import { TypeOrmRepository } from './typeOrmRepository';

@injectable()
@EntityRepository(UserEntity)
export class UserTypeOrmRepository extends TypeOrmRepository<UserEntity> {
  constructor() {
    super(getRepository(UserEntity));
  }

  async addUser(user: UserEntity): Promise<UserEntity> {
    const userToSave = this.getRepository().create(user);
    const savedUser = await this.save(userToSave);
    return savedUser;
  }

  async deleteUser(id: string): Promise<boolean> {
    const result = await this.getRepository().update(id, { isDeleted: true });
    return result.affected === 1 ? true : false;
  }

  async getUsers(name: string, nickName: string): Promise<Array<any>> {
    const builder = this.getRepository().createQueryBuilder('user');
    builder.where(
      'user.isDeleted != true AND (user.firstName LIKE :name OR user.lastName LIKE :name) AND user.nickName LIKE :nick',
      { name: `%${name ? name : ''}%`, nick: `%${nickName ? nickName : ''}%` }
    );
    const usersResult = await builder.getMany();
    const users: Array<any> = [];
    usersResult.forEach((user) => {
      users.push({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        nickName: user.nickName,
        totalAttendance: user.totalAttendance,
      });
    });

    return users;
  }

  async getUser(id: string): Promise<UserEntity | undefined> {
    return await this.userExist(id)? await this.findById(id): undefined;
  }
  async userExist(id: string): Promise<boolean> {
    const builder = this.getRepository().createQueryBuilder('user');
    builder.where('user.isDeleted != true AND user.id = :id', { id: id });
    const userResult = await builder.getOne();
    return userResult ? true : false;
  }
}
