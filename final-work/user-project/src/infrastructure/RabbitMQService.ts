import { inject, injectable } from 'inversify';
import { MessageBroker } from '../core/interfaceAdapters/messageBroker';
import amqp = require('amqplib');
import { Channel, Connection, ConsumeMessage, Options } from 'amqplib';
import config from '../config/config';
import { UserTypeOrmRepository } from './userRepository';
import { IDENTIFIERS } from '../identifiers';

@injectable()
export class RabbitMQService implements MessageBroker {
  conn: Connection;
  channel: Channel;
  q: string;
  constructor(
    @inject(IDENTIFIERS.USER_REPOSITORY)
    private userRepository: UserTypeOrmRepository
  ) {}
  async setupConnection(): Promise<void> {
    this.conn = await amqp.connect({
      protocol: 'amqp',
      hostname: config.RABBITMQ_HOST,
      password: config.RABBITMQ_PASSWORD,
      username: config.RABBITMQ_USER,
      port: config.RABBITMQ_PORT,
    } as Options.Connect);
    console.log('⚡️[RabbitMQ]: Connected to instance.');

    this.channel = await this.conn.createChannel();
    for (const queue of config.QUEUES) {
      await this.channel.assertQueue(queue, { durable: false });
    }
  }

  async sendMessage(message: string, queueName: string): Promise<boolean> {
    if (this.channel.sendToQueue(queueName, Buffer.from(message))) {
      console.log('[RabbitMQ]: Send: %s', message);
      return true;
    }
    return false;
  }

  async consumeUpdateUser() {
    await this.channel.consume(
      config.QUEUE_UPDATE_USER,
      (msg: ConsumeMessage) => {
        const result = msg.content.toString();
        console.log(`[RabbitMQ]: Received: ${result}`);
        const { userId, totalAttendance } = JSON.parse(result);
        this.userRepository
          .getRepository()
          .update(userId, { totalAttendance: totalAttendance });
        this.channel.ack(msg);
      }
    );
  }
}