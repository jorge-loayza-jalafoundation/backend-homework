import { injectable } from 'inversify';
import { ConnectionOptions, createConnection } from 'typeorm';
import config from '../config/config';
import { UserEntity } from './dbEntities/user.entity';
import DbConnection from '../core/interfaceAdapters/db/dbConnection';

@injectable()
export class TypeOrmConnection implements DbConnection {
  async initialize(): Promise<void> {
    await createConnection({
      type: config.DB_TYPE,
      host: config.DB_HOST,
      port: config.DB_PORT,
      username: config.DB_USER,
      password: config.DB_PASSWORD,
      database: config.DB_NAME,
      entities: [UserEntity],
      synchronize: config.DB_SYNCRO,
      logging: false,
    } as ConnectionOptions)
      .then(async (connection) => {
        console.log(`⚡️[database]: Connected to db`);
        // const user = new User();
        // user.name = 'test name';
        // user.nickName = 'Nick name'
        // return connection.manager.save(user).then((user) => {
        //   console.log('User: ', user);
        // });
      })
      .catch((error) =>
        console.log('⚡️[database]: ', error)
      );
  }
}
