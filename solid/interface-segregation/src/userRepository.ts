import DeleteInterface from './interfaces/deleteInterface';
import GetInterface from './interfaces/getInterface';
import InsertInterface from './interfaces/insertInterface';
import Repository from "./interfaces/repository";
import UpdateInterface from './interfaces/updateInterface';
import User from './user';

export default class UserRepository implements InsertInterface<User>, UpdateInterface<User>, GetInterface<User>,
    DeleteInterface<User>{

    insert(user: User) {

    }

    update(user: User) {

    }

    get(): User {
        return new User('1');
    }

    delete(id: string): void {

    }
}