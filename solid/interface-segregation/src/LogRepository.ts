import EventLog from "./eventLog";
import GetInterface from './interfaces/getInterface';
import Repository from "./interfaces/repository";
import User from './user';

export default class LogRepository implements GetInterface<EventLog> {

    //  insert(user: EventLog) {
    //      //none
    //  }

    // update(user: EventLog) {
    //     //none
    // }

    get(): EventLog {
        return new EventLog('1', 'test message');
    }

    // delete(id: string): void {
    //     //none
    // }
}