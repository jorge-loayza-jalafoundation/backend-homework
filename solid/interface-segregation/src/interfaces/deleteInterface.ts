export default interface DeleteInterface<T> {

    delete(id: string): void;
}