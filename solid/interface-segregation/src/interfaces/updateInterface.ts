export default interface UpdateInterface<T> {

    update(entity: T);

}