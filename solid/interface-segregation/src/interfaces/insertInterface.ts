export default interface InsertInterface<T> {

    insert(entity: T);

}