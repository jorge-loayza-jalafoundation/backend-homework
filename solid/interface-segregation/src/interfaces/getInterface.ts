export default interface GetInterface<T> {

    get(): T;

}