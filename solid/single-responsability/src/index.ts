import Book from "./book";
import Invoice from "./invoice";
import InvoicePersistant from "./invoicePersistant";
import InvoicePrinter from './invoicePrinter';

const book = new Book("Clean Code", "Bob", 1995, 49, "SD-456-ASD");
const invoice = new Invoice(book, 1, 50, 0.14);
invoice.calculateTotal();

const invoicePrinter = new InvoicePrinter();
invoicePrinter.printInvoice(invoice);

const invoicePersistant = new InvoicePersistant();
invoicePersistant.saveToFile(invoice);