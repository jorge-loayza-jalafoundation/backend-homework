export default interface LampInterface{
  turnOn(): void;
  turnOff(): void;
  getStatus(): boolean;
}