import LampInterface from '../lamp-interface';
import Button from './button';
import Lamp from './lamp';



const lamp:LampInterface = new Lamp();
const button = new Button(lamp);

button.onButtonListener();

