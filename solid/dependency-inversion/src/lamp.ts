import LampInterface from '../lamp-interface';

export default class Lamp implements LampInterface{

    private status: boolean;
    constructor() {
      this.status = false;
    }

    turnOn(): void {

        this.status = true;

        console.log('Lamp is ON');
    }

    turnOff(): void {

        this.status = false;

        console.log('Lamp is OFF');

    }

    getStatus(): boolean {

        return this.status;

    }

}