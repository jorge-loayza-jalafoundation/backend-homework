import LampInterface from '../lamp-interface';


export default class Button {



    constructor(protected lamp: LampInterface) {

    }



    onButtonListener() {

        if (this.lamp.getStatus()) {

            this.lamp.turnOff()

        } else {

            this.lamp.turnOn();

        }

    }

}