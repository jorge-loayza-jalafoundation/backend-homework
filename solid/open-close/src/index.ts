import User from "./user";
import NotificationCenterExtended from './notificationCenterExtended';

const user = new User('Bob');
const notificationCenter: NotificationCenterExtended = new NotificationCenterExtended(user, 'testMessage');

notificationCenter.notifyByEmail();

notificationCenter.notifyByFacebook();