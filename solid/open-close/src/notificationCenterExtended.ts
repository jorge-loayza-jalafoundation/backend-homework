
import NotificationCenter from './notificationCenter';
import User from "./user";


export default class NotificationCenterExtended extends NotificationCenter {

    constructor( user: User, message: string) {
        super(user, message);
    }

    notifyByFacebook(){
        console.log(`Notify by Facebook to ${this.user.name} message: ${this.message}...`);
    }

}
