
function getDoctors() {
  return {
    system: 'San Pedro',
    doctors: [
      {
        name: 'Doctor 4',
      },
      {
        name: 'Doctor 5',
      },
      {
        name: 'Doctor 6',
      },
    ],
  };
}

export { getDoctors };
