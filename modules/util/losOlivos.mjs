
function getDoctors() {
  return {
    system: 'Los Olivos',
    doctors: [
      {
        name: 'Doctor 1',
      },
      {
        name: 'Doctor 2',
      },
      {
        name: 'Doctor 3',
      },
    ],
  };
}

export { getDoctors };
