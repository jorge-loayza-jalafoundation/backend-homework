
export default async function getAvalaibleDoctors() {
  let moduleSpecifier = '';
  const time = new Date().getHours();
  if ((time > 6 && time < 19)) {
    moduleSpecifier = './losOlivos.mjs';
  } else {
    moduleSpecifier = './sanPedro.mjs';
  }
  const module = await import(moduleSpecifier);
  return module.getDoctors();
}
