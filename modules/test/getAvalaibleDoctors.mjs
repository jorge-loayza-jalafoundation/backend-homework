import { expect } from 'chai';
import sinon from 'sinon';
import getAvalaibleDoctors from '../util/getAvalaibleDoctors.mjs';

let clock;

afterEach(function () {
  clock.restore();
});

describe('GetAvalaibleDoctors test', function () {
  it('Should load Los Olivos module', async function () {

    clock = sinon.useFakeTimers(new Date(2021, 10, 30, 16, 0).getTime());

    const doctors = await getAvalaibleDoctors();

    expect(doctors.system).to.be.equal('Los Olivos');

  });

  it('Should load San Pedro module', async function () {
    clock = sinon.useFakeTimers(new Date(2021, 10, 30, 20, 0).getTime());

    const doctors = await getAvalaibleDoctors();

    expect(doctors.system).to.be.equal('San Pedro');

  });
});
