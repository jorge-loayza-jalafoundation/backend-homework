# Backend Homework 

This repository includes all the homework given in backend classes:

* [Architecture - N-Tier Architecture](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/n-tier-architecture) 
* [Javascript Collections](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/collections) 
* [BigInt Serialization and Deserialization](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/bigint)
* [Modules](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/modules) 
* [Dependency Inversion Principle](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/solid/dependency-inversion) 
* [Single Responsability Principle](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/solid/single-responsability) 
* [Open Close Principle](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/solid/open-close) 
* [Liskov Substitution Principle](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/solid/liskov) 
* [Interface Segregatation Principle](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/solid/interface-segregation) 
* [REST - Error Response](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/rest-presentation) 
* [RabbitMQ - Headers Exchange](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/headers-exchange) 

# Chess API Project

* [Chess API](https://gitlab.com/jorge.loayza/backend-homework/-/tree/main/chess-api) 



