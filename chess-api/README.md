# Chess API

# Requirements

* NodeJS 

# Installation

To install the dependencies run the following command:

```sh
$ npm install
```

# Execution

To run the api in development mode just enter the following command:

```sh
$ npm run start:dev
```

To start the api just enter the following command:

```sh
$ npm run start
```

# C4 Level 1

![C4 Level 1](./docs/c4-model-level1.png)

# C4 Level 2

![C4 Level 2](./docs/c4-model-level2.png)

# C4 Level 3

![C4 Level 3](./docs/c4-model-level3.png)

# Chess Pieces

Piece:

- "p" for Pawn
- "n" for Knight
- "b" for Bishop
- "r" for Rook
- "q" for Queen
- "k" for King

Color

- "w" or "b"