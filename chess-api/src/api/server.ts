import { injectable } from 'inversify';
import { container } from '../container';
import { InversifyExpressServer } from 'inversify-express-utils';
import * as bodyParser from 'body-parser';
import { Application } from 'express';

export interface IServer {
  start(): void;
}

@injectable()
export class Server implements IServer {
  server: InversifyExpressServer;
  start(): void {
    this.server = new InversifyExpressServer(container);
    this.server.setConfig((app: Application) => {
      // add body parser
      app.use(
        bodyParser.urlencoded({
          extended: true,
        })
      );
      app.use(bodyParser.json());
    });

    const app = this.server.build();
    app.listen(process.env.PORT || 3000);
  }
}
