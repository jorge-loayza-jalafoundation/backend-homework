import { TYPES } from '../types';
import { ChessService } from '../app/chess';

import * as express from 'express';
import {
  interfaces,
  controller,
  httpGet,
  request,
  response,
  next,
} from 'inversify-express-utils';
import { inject } from 'inversify';

@controller('/movement')
export class HTTPController implements interfaces.Controller {
  constructor(@inject(TYPES.ChessService) private chessService: ChessService) {}

  @httpGet('/')
  private index(
    @request() req: express.Request,
    @response() res: express.Response,
    @next() next: express.NextFunction
  ) {
    try {
      const { current_position, piece, color } = req.body;
      const movements = this.chessService.getMovement({
        current_position,
        piece,
        color,
      });
      res.status(200).json(movements);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
}
