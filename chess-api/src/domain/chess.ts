import { Entity } from './entity';

export interface UnmarshalledItem {
  id?: string;
}

export class Chess extends Entity<UnmarshalledItem> {
  private constructor(props: UnmarshalledItem) {
    const { id, ...data } = props;
    super(data, id);
  }

  public static create(props: UnmarshalledItem): Chess {
    const instance = new Chess(props);
    return instance;
  }

  public unmarshal(): UnmarshalledItem {
    return {
      id: this.id,
    };
  }

  get id(): string {
    return this._id;
  }
}
