const TYPES = {
  Server: Symbol.for('Server'),
  HTTPController: Symbol.for('HTTPController'),
  HTTPRouter: Symbol.for('HTTPRouter'),

  ChessService: Symbol.for('ChessService'),
  ChessRepository: Symbol.for('ChessRepository'),
}

export { TYPES }
