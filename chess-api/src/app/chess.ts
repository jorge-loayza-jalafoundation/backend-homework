import { injectable } from 'inversify';
import { Chess, PieceType, Square } from 'chess.js';
import AppError from '../lib/AppError';
interface IRequest {
  current_position: Square;
  piece?: PieceType;
  color?: 'b' | 'w';
}

interface IResponse {
  id: string;
  current_position: Square;
  piece: PieceType;
  color: 'b' | 'w';
  legal_moves: string[];
}
@injectable()
export class ChessService {
  public getMovement({
    current_position,
    piece = 'n',
    color = 'w',
  }: IRequest): any {
    const chess = new Chess();
    chess.clear();

    if (color === 'b') {
      chess.put({ type: 'q', color: 'w' }, 'e4');
      chess.move({ from: 'e4', to: 'e5' });
      chess.remove('e5');
    }
    const formattedPosition = current_position.toLowerCase() as Square;

    const isValidPosition = chess.put(
      { type: piece, color },
      formattedPosition
    );

    if (!isValidPosition) {
      throw new AppError(
        'Invalid current position, please check the parameters.'
      );
    }

    const moves = chess.moves({
      square: formattedPosition,
    });

    const algebraicMoves = moves.map((move) => {
      if (move.includes('=')) {
        return move.substr(0, 2).toUpperCase();
      }

      if (move.endsWith('+') || move.endsWith('#')) {
        return move.substr(-3, 2).toUpperCase();
      }
      return move.substr(-2).toUpperCase();
    });
    const legal_moves = Array.from(new Set(algebraicMoves));
    return {
      current_position,
      piece,
      color,
      legal_moves,
    };
  }
}
