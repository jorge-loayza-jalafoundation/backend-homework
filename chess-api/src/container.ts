import 'reflect-metadata';

import { Container } from 'inversify';
import { TYPES } from './types';
import { HTTPController } from './api/controller';

import { Server, IServer } from './api/server';

import { ChessService } from './app/chess';

const container = new Container();

container.bind(TYPES.HTTPController).to(HTTPController).inSingletonScope();
container.bind<IServer>(TYPES.Server).to(Server).inSingletonScope();

container.bind(TYPES.ChessService).to(ChessService);

export { container };
