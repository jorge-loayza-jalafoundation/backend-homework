---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](images/layers.png)

# **Error Representation ⚠️**

---

# Rule: A consistent form should be used to represent errors

![](images/img1.png)

---

# Rule: A consistent form should be used to represent error responses


![bg left 100%](images/img2.png)

---

# Rule: Consistent error types should be used for common error conditions

---