# Generate Slides

To generete the slides just enter the following command in the project folder:

```sh
$ npx @marp-team/marp-cli@latest slides.md
```
> To see the slides just open the the `slides.html` file in the browser