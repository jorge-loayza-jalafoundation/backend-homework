import amqp = require("amqplib/callback_api");

amqp.connect(
  {
    protocol: "amqp",
    hostname: "localhost",
    port: 5672,
    username: "admin",
    password: "123456",
  },
  function (error0, connection) {
    if (error0) {
      throw error0;
    }
    
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      let queue = "ReportExcel";
      channel.assertQueue(queue, {
          durable:true
      });

      channel.assertExchange("header.exchange", "headers", {
        durable: true,
      });

      console.log(`Waiting for messages...`);
      channel.consume(queue, function(message){
        console.log(`Message received: ${message?.content.toString()}`);
        
      },
      {
          noAck:true
      });

    });
    // setTimeout(function(){
    //     connection.close();
    //     process.exit(0);
    // }, 500)
  }
);